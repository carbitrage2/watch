declare interface AuthRequest {
    uid: number;
    password: string;
}