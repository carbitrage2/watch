declare interface Credentials {
    login: string;
    password: string;
}

declare interface Cookie {
    domain: string;
    httpOnly: boolean;
    name: string;
    path: string;
    secure: boolean;
    value: string;
}