declare interface CommonResponse {
    code: string;
    message: string;
}