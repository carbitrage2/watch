import Navbar from "./navbar.component";
import AuthModal from './auth.component';

class Page {
    public navbar: Navbar = new Navbar;
    public authModal: AuthModal = new AuthModal;

    async open (path: string): Promise<void> {
        await browser.url(path);
    }
}

export default Page;
