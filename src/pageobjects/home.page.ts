import Page from "./page";

class HomePage extends Page {
    async open(): Promise<void> {
        await super.open('/');
    }
}

export default new HomePage;