import homePage from '../pageobjects/home.page';
import { getEnvCredentials } from '../helpers/auth.helper';

describe('Auth', function () {
    const credentials: Credentials = getEnvCredentials();

    it('user id should contain 9 digits', async function () {
        await homePage.open();
        await homePage.navbar.clickUserMenu();
        await homePage.authModal.setUserID('123');
        browser.pressTab();
        homePage.authModal.checkForSpecificError('User ID should contain 9 digits');
    });

    it('accepts request for password', async function () {
        await homePage.open();
        await homePage.navbar.clickUserMenu();
        await homePage.authModal.setUserID(credentials.login);
        await homePage.authModal.clickSendPassword();
        homePage.authModal.passwordInputIsDisplayed();
    });

    it('password should contain 4 digits', async function () {
        await homePage.open();
        await homePage.navbar.clickUserMenu();
        await homePage.authModal.setUserID(credentials.login);
        await homePage.authModal.clickSendPassword();
        await homePage.authModal.setPassword('12');
        browser.pressTab();
        homePage.authModal.checkForSpecificError('Password should contain 4 digits');
    });

    it('log in', async function () {
        await homePage.open();
        await homePage.navbar.clickUserMenu();
        await homePage.authModal.setUserID(credentials.login);
        await homePage.authModal.clickSendPassword();
        await homePage.authModal.setPassword(credentials.password);
        await homePage.authModal.clickLogIn();
        homePage.navbar.loggedIn(true);
    });

    it('log out', async function () {
        await homePage.open();
        browser.refresh();
        homePage.navbar.loggedIn(true);
        await homePage.navbar.clickUserMenu();
        await homePage.navbar.chooseLogOutOption();
        homePage.navbar.loggedIn(false);
    });
});