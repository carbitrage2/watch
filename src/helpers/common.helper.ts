async function shuffle(array: Array<any>): Promise<any> {
    return new Promise(async resolve => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        resolve(array);
    })
}

export { shuffle };