import { config as dotenvconfig } from 'dotenv';
import axios, { AxiosResponse, AxiosError } from 'axios';

function getEnvCredentials(): Credentials {
    return { login: process.env.USER_LOGIN, password: process.env.USER_PASSWORD } as Credentials;
}

async function getDomain(): Promise<string> {
    return new Promise(async resolve => {
        const url: string = await browser.getUrl();
        const domain: string = url.toString().split('https://')[1].split('/')[0];
        resolve(domain);
    })
}

async function getApiUrl(): Promise<string> {
    return new Promise(async resolve => {
        const url: string = await browser.getUrl();
        const domain: string = url.toString().split('https://')[1].split('/')[0];
        if (domain.includes('www.')) {
            resolve('api' + domain.split('www')[1]);
        } else {
            resolve('api.' + domain);
        }
    });
}

async function logIn(): Promise<void> {
    return new Promise(async resolve => {
        dotenvconfig();
        console.log(`URLNOW: ${await browser.getUrl()}`);
        if (process.env.USER_LOGIN && process.env.USER_PASSWORD) {
            const login: number = +process.env.USER_LOGIN;
            const password: string = process.env.USER_PASSWORD;
            const body: AuthRequest = { uid: login, password: password };
            const headers: Object = { headers: { 'Content-Type': 'application/json' } };
            const domain: string = await getDomain();
            const api: string = await getApiUrl();
            console.log(`USER NAME: ${login}, PASSWORD: ${password}`);
            console.log(`DOMAIN ROUTE: ${domain}`);
            console.log(`API URL: ${api}`);
            await axios.post(`https://${api}/auth`, body, headers)
                .then((response: AxiosResponse) => {
                    const data: CommonResponse = response.data as CommonResponse;
                    const cookie: Cookie = {
                        domain: domain,
                        httpOnly: false,
                        name: 'token',
                        path: '/',
                        secure: false,
                        value: data.message
                    }
                    browser.addCookie(cookie);
                    console.log('COOKIEBEATCH');
                    console.log(cookie);
                    browser.refresh();
                    resolve();
                })
                .catch((error: AxiosError) => {
                    console.log(`MONEEEY! ${error.message}, ${error.response?.data}`);
                    resolve();
                });
        }
    })
}

export { getEnvCredentials, logIn };