import { Config } from 'webdriverio';
import { config as dotenvconfig } from 'dotenv';

const config: Config = {
    runner: 'local',
    specs: [
        './src/specs/**/*.ts'
    ],
    maxInstances: 1,
    capabilities: [{
        maxInstances: 1,
        browserName: 'chrome',
        acceptInsecureCerts: true
    }],
    logLevel: 'info',
    bail: 0,
    baseUrl: 'https://www.carbi.me',
    waitforTimeout: 5000,
    connectionRetryTimeout: 15000,
    connectionRetryCount: 3,
    services: ['chromedriver'],
    framework: 'mocha',
    reporters: [
        'spec',
        [
            'allure', {
                outputDir: 'allure-results',
                disableWebdriverStepsReporting: true,
                disableWebdriverScreenshotsReporting: true,
            }
        ]
    ],

    before: function (capabilities: WebDriver.DesiredCapabilities, specs: string[]) {
        dotenvconfig();
        browser.addCommand('pressTab', function (): void {
            browser.keys(['Tab']);
        });
    }
}

export { config }